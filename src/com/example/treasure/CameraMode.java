
package com.example.treasure;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.NameValuePair;

import com.example.treasure.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.media.ExifInterface;
import android.net.Uri;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CameraMode extends Activity {

int WiFi_Level;
String BSSID;
String ChestId;
double latitude;
double longitude;
Button ButtonClick;
Button ButtonClick2;
ImageView myImageView;
TextView textView;
String answer;
Bitmap thumbnail = null;
File raw;
boolean IsActiveThread=false;
ImageUpload IU = null;
int i =0;
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    
    //TERIMA INTENT
    Intent intent = getIntent();
    
    Bundle bundelTracking = intent.getExtras();
    
    BSSID = bundelTracking.getString("bssid");
    latitude = bundelTracking.getDouble("latitude");
    longitude = bundelTracking.getDouble("longitude");
    ChestId = bundelTracking.getString("chestid");
    WiFi_Level = bundelTracking.getInt("level");
    
    
    setContentView(R.layout.activity_demo);
    File folder = new File(Environment.getExternalStorageDirectory() + "/RadarApp");
    if(folder.exists() == false){
        folder.mkdirs();
    }
    
    textView =(TextView) findViewById(R.id.ServerResponse);
    textView.setVisibility(TextView.INVISIBLE);
    
    ButtonClick =(Button) findViewById(R.id.Camera);
    ButtonClick.setOnClickListener(new OnClickListener (){
        @Override
        public void onClick(View view)
        {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            // request code
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(new File(Environment.getExternalStorageDirectory(),"/RadarApp/Attachment.jpg")));
            i++;
            startActivityForResult(cameraIntent, 1001);
        }
    });


    ButtonClick2 = (Button) findViewById(R.id.Submit);
    ButtonClick2.setVisibility(Button.INVISIBLE);
    ButtonClick2.setOnClickListener(new OnClickListener (){
        @Override
        public void onClick(View view)
        { //kirim gambar yang dibuat
        if (thumbnail== null){
        	Log.d("Submit", "There Is No Picture,Please Take Pitcure");
        }
        else{
        	try {
        		ExifInterface exif;
				exif = new ExifInterface(raw.getAbsolutePath());
		        Log.d("check geoTag", exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE));
		        Log.d("check geoTag", exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	Log.d("Submit", "mulai connect ke server");
        	Submit();
        	while(IsActiveThread){
        	///////	
        	}
        	Toast.makeText(getApplicationContext(), answer, Toast.LENGTH_LONG).show();
        	
        	Intent intent = new Intent(getApplicationContext(),RadarMain.class);
        	startActivity(intent);
        	
        	finish();
        }
        }
    });
  myImageView = (ImageView) findViewById(R.id.Hasil);
  myImageView.setVisibility(ImageView.INVISIBLE);  
}


public void geoTag(String filename, double latitude, double longitude){
    

    try {
    	ExifInterface exif = new ExifInterface(filename);
    	exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, GPS.convert(latitude));
    	exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, GPS.latitudeRef(latitude));
    	exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, GPS.convert(longitude));
    	exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, GPS.longitudeRef(longitude));
    	exif.saveAttributes();
        Log.d("geoTag", exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE));
        Log.d("geoTag", exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE));
    } catch (IOException e) {
        Log.d("geoTag", e.getLocalizedMessage());
    } 

    }


public void Submit(){
	IU = new ImageUpload(ChestId,raw,BSSID,WiFi_Level,IsActiveThread,answer,this);
	IU.start();
}

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) 
{    if(resultCode == RESULT_OK)
    {
	//buka file gambar di simpan 
	File f = new File(Environment.getExternalStorageDirectory(),"/RadarApp/Attachment.jpg");
	//masukan ke bitmap
	Bitmap  hasil = BitmapFactory.decodeFile(f.getAbsolutePath());
	//lakukan resize
	Bitmap resize = Bitmap.createScaledBitmap(hasil, 480, 640, true);
	thumbnail = resize;
	//masukan hasil resize ke file
	try {
		OutputStream outStream = null;
		File newf = new File(f.getParent(),"resize"+i+".jpg");
		outStream = new FileOutputStream(newf);
	    thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
	    outStream.flush();
	    outStream.close();
	    raw = newf;
	}
	    catch(Exception e)
	    {
	    	Log.d("store file","failed");
	    }
   
	//GeoTagging
	Log.d("geoTag",raw.getAbsolutePath());
	//PAKAI ELEMENT DUMMY
	Log.d("geoTag Latitude , Longitude",""+latitude+" , "+longitude);
	geoTag(raw.getAbsolutePath(),latitude, longitude);
	Log.d("Broad Cast",raw.getParent());
	sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(raw.getParent())));
	//display di imageview 
    myImageView.setImageBitmap(thumbnail);
    myImageView.setVisibility(ImageView.VISIBLE);
    ButtonClick2.setVisibility(Button.VISIBLE);
    }
    else 
    {
        Toast.makeText(CameraMode.this, "Picture NOt taken", Toast.LENGTH_LONG).show();
    }
    super.onActivityResult(requestCode, resultCode, data);
}

@Override
protected void onResume(){
	super.onResume();
	
	
}

@Override
protected void onPause(){
	super.onPause();
	
}

}