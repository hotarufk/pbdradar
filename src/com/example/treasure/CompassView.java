package com.example.treasure;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class CompassView extends View{
	
	private Paint paint;
	private float position = 0;
	Double Lat;
	Double Long;
	ArrayList<Double> Deg =new ArrayList<Double>();
	ArrayList<Double> Dis=new ArrayList<Double>();
	  public CompassView(Context context) {
	    super(context);
	    init();
	  }

	  private void init() {
	    paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setStrokeWidth(4);
	    paint.setTextSize(25);
	    paint.setStyle(Paint.Style.STROKE);
	    paint.setColor(Color.BLACK);
	  }

	  @Override
	  protected void onDraw(Canvas canvas) {
	    int xPoint = getMeasuredWidth() / 2;
	    int yPoint = getMeasuredHeight() / 2;

	    float radius = (float) (Math.max(xPoint, yPoint) * 0.6);
	    canvas.drawCircle(xPoint, yPoint, radius, paint);
	    for(int i = 0;i<4;i++){
	    	canvas.drawCircle(xPoint, yPoint, radius*i/4, paint);
	    }

	    // Using quite near approximation of Phi.
	    canvas.drawLine(xPoint,
	        yPoint + radius,
	        (float) (xPoint + radius
	            * Math.sin((double) (-position) / 180 * 3.1421)),
	        (float) (yPoint - radius
	            * Math.cos((double) (-position) / 180 * 3.1421)), paint);
	    canvas.drawLine(xPoint - radius,
		        yPoint,
		        xPoint + radius,
		        yPoint, paint);
		    

	  
	  for(int i=0;i<Dis.size();i++){
		  canvas.drawPoint((float)(xPoint + Dis.get(i)*Math.sin((double)(Deg.get(i)/180*3.1421))*radius/100),(float)(yPoint - Dis.get(i)*Math.cos((double)(Deg.get(i)/180*3.1421))*radius/100) , paint);
		  canvas.drawText(String.valueOf(i+1),(float)(xPoint + Dis.get(i)*Math.sin((double)(Deg.get(i)/180*3.1421))*radius/100+10), (float)(yPoint - Dis.get(i)*Math.cos((double)(Deg.get(i)/180*3.1421))*radius/100), paint);
	  }
	  
	  }

	  public void updateData(Double Latitude,Double Longitude,ArrayList<Double> Distance,ArrayList<Double> Degree) {
	   Lat = Latitude;
	   Long =Longitude;
	   Dis =Distance;
	   Deg = Degree;
	    invalidate();
	  }

}