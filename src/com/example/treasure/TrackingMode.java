package com.example.treasure;

import java.util.logging.Level;

import com.example.treasure.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class TrackingMode extends Activity implements LocationListener{

	//CONSTANTA
	public static final double DIAMETER_BUMI = 6371000;//DALAM METER
	
	
	
	//SENSOR
	private static SensorManager sensorService;
	private Sensor sensor;
	
	//LOKASI
	private LocationManager locationManager;
	private String provider;
	private PBDView pBDView;
	
	//LAYOUT
	private FrameLayout frame;
	private RelativeLayout relativeLay;
	
	//BUTTON GANTI MODE
	public Button radarMode;
	public Button cameraMode;
  
	//POSISI KITA
	private double lat;
	private double longit;
	
	//POSISI HARTA KARUN
	private double hLat;
	private double hLongit;
	
	//PERBEDAAN JARAK DALAM DEGREE
	private double dLat;
	private double dLongit;
	
	//PERBEDAAN JARAK DALAM METER
	private double dist;
	private float arrDist[] = new float[200];
	
	//PARAMETER KIRIM
	public String bssid;
	public String chestid;
	
	//KELAS DETEKTOR WIFI
	public SuperWifi wiFi;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Intent intent = getIntent();
		Bundle bundel = intent.getExtras();
		
		double bLatitude = bundel.getDouble("latitude");
		double bLongitude = bundel.getDouble("longitude");
		
		lat = bLatitude;
		longit = bLongitude;
		
		bssid = intent.getStringExtra("bssid");
		double degree = bundel.getDouble("degree");
		double distance = bundel.getDouble("distance");
		distance =distance /1000;
		
		Location loc = new Location("S");
		loc.setLatitude(bLatitude);
		loc.setLongitude(bLongitude);
		
		Location locAfter;
		
		locAfter = GetDestinationPoint(loc, degree, distance);
		
		hLat = locAfter.getLatitude();
		hLongit = locAfter.getLongitude();
		
//DEBUG DISTANCE AWAL
float[] results = new float[4];
Location.distanceBetween(lat, longit, hLat, hLongit, results);

Log.d("DISTANCE ONCREATE",""+results[0]);
		

		chestid = bundel.getString("id");
		
//TODO
//DUMMY INIZIALITATION
//hLat = -6.890608;
//hLongit = 107.610008;
//bssid = "f8:1a:67:b8:08:d7";
//chestid = "820381966e9d536f0bbfc52f8d6f7e1f";

		super.onCreate(savedInstanceState);
		
		pBDView = new PBDView(this);
		
		setContentView(pBDView);
		
		//INISIALISIASI BUTTON
		initLayout();
		
		//HANDLE SENSOR ORIENTATION
		sensorService = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorService.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		if(sensor !=null){
			sensorService.registerListener(mySensorEventListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		}else{
			Toast.makeText(this, "Tidak memiliki sensor orientation, tidak bisa memakai aplikasi.", Toast.LENGTH_LONG).show();
			finish();
		}
		
		//HANDLE LOKASI MANAGER
		//GET LOCATIONMANAGER
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		//SELECT DEFAULT PROVIDER
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		Location location = locationManager.getLastKnownLocation(provider);
		
		//INITIALIZE THE LOCATION FIELDS
		if(location!=null){
			
		}else{
			
		}
		
		//WIFI MODE
		wiFi = new SuperWifi(this, bssid);
		distSearch();
		pBDView.updateData(dist);
	}
	
	//MEMBUAT SENSOREVENTLISTENER
	private SensorEventListener mySensorEventListener = new SensorEventListener() {
		
		@Override
		public void onSensorChanged(SensorEvent event) {
			
			//double tanTheta = dLongit/dLat;
			//double theta = Math.atan(tanTheta)*180/Math.PI;
			
			float theta;
			
			arrDist[2] = 599;
			Location.distanceBetween(lat, longit, hLat, hLongit, arrDist);
			
			if(arrDist[2]!= 599){
				theta = arrDist[2];
			}else{
				theta = arrDist[1];
			}
			
			float azimuth = event.values[0];
			float derajatKeHarta = (float) (theta + azimuth);
			
			
			Log.d("DISTANCE SKRG", ""+dist);
			Log.d("Azimuth",""+azimuth);
			Log.d("sudut",""+theta);
			
			pBDView.updateData(derajatKeHarta);
		}
		
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.trackingmain, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		super.onOptionsItemSelected(item);
		switch(item.getItemId()){
		case R.id.camera_mode:
			if(wiFi.found){
				//MAKE AN INTENT
				Intent intent;
				intent = new Intent(this,CameraMode.class);
				
				//ISI INTENT
				intent.putExtra("bssid", bssid);
				intent.putExtra("latitude", lat);
				intent.putExtra("longitude", longit);
				intent.putExtra("chestid", chestid);
				intent.putExtra("level", wiFi.level);
				//START ACTIVITY
				startActivity(intent);
				finish();
				break;
			}else{
				
				Intent intent;
				intent = new Intent(this,CameraMode.class);
				
				intent.putExtra("bssid", bssid);
				intent.putExtra("latitude", lat);
				intent.putExtra("longitude", longit);
				intent.putExtra("chestid", chestid);
				intent.putExtra("level", 100);
				startActivity(intent);
				finish();
				break;
			}
		case R.id.radar_mode:
			Intent intent = new Intent(this,RadarMain.class);
			startActivity(intent);
			finish();
			break;
		}
		return true;
	}
	
	//NYALAIN SENSOR KALAU RESUME
	@Override
	protected void onResume(){
		super.onResume();
		locationManager.requestLocationUpdates(provider, 400, 1, this);
		if(sensor !=null){
			sensorService.registerListener(mySensorEventListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		}
		wiFi.start();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		locationManager.removeUpdates(this);
		sensorService.unregisterListener(mySensorEventListener);
		wiFi.release();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		locationManager.removeUpdates(this);
		sensorService.unregisterListener(mySensorEventListener);
		wiFi.release();
	}
	
	@Override
	public void onLocationChanged(Location location){
		
		
		
		lat = location.getLatitude();
		longit = location.getLongitude();
		pBDView.updateData(longit, lat);
				
		distSearch();
		Log.d("Location changed to", ""+dist);
		pBDView.updateData(dist);
	}

	@Override
	public void onProviderDisabled(String provider) {
		Toast.makeText(this, "Anda Mematikan Provider", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onProviderEnabled(String provider) {
		Toast.makeText(this, "Anda Menyalakan Provider", Toast.LENGTH_SHORT).show();
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}
	
	//METHOD LAIN LAIN
	public void initLayout(){
		//INISIALISASI LAYOUT
		frame = new FrameLayout(this);
		relativeLay = new RelativeLayout(this);
		
		DisplayMetrics metrics;
		
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		
		//MEMBUAT BUTTON BARU
	    radarMode = new Button(this);
	    cameraMode = new Button(this);
		
	    //SET NAMA BUTTON
	    radarMode.setText("Radar Mode");
	    cameraMode.setText("CameraMode");
	    
	    //MENGAMBIL LEBAR DAN PANJANG LAYAR
	    int lebar = metrics.widthPixels;
	    int panjang = metrics.heightPixels;
	    
	    //SET POSISI BUTTON
	    radarMode.setX(120);
	    radarMode.setY(120);
	    
	    cameraMode.setX(240);
	    cameraMode.setY(240);
	    
	    radarMode.setVisibility(Button.VISIBLE);
	    cameraMode.setVisibility(Button.VISIBLE);
	    
	    relativeLay.addView(radarMode);
	    relativeLay.addView(cameraMode);
	    
	    //MASUKIN KE FRAME UTAMA
	    frame.addView(relativeLay);
	    relativeLay.setVisibility(RelativeLayout.VISIBLE);
	    frame.setVisibility(FrameLayout.VISIBLE);
	}
	
	
	public void distSearch(){

		//MENCARI DISTANCE
		Location.distanceBetween(lat, longit, hLat, hLongit, arrDist);
		dist = arrDist[0];
	}

	public static Location GetDestinationPoint(Location startLoc, double bearing, double depth) 
	{ 
	    Location newLocation = new Location("newLocation");

	    double radius = 6371.0; // earth's mean radius in km 
	    double lat1 = Math.toRadians(startLoc.getLatitude()); 
	    double lng1 = Math.toRadians(startLoc.getLongitude()); 
	    double brng = Math.toRadians(bearing); 
	    double lat2 = Math.asin( Math.sin(lat1)*Math.cos(depth/radius) + Math.cos(lat1)*Math.sin(depth/radius)*Math.cos(brng) ); 
	    double lng2 = lng1 + Math.atan2(Math.sin(brng)*Math.sin(depth/radius)*Math.cos(lat1), Math.cos(depth/radius)-Math.sin(lat1)*Math.sin(lat2)); 
	    lng2 = (lng2+Math.PI)%(2*Math.PI) - Math.PI;  

	    // normalize to -180...+180 
	    if (lat2 == 0 || lng2 == 0) 
	    {
	        newLocation.setLatitude(0.0);
	        newLocation.setLongitude(0.0);
	    }
	    else
	    {
	        newLocation.setLatitude(Math.toDegrees(lat2));
	        newLocation.setLongitude(Math.toDegrees(lng2));
	    }

	    return newLocation;
	};
}
