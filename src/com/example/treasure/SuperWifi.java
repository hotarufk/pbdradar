package com.example.treasure;

import java.util.List;

import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.Date;

public class SuperWifi extends Thread {

	public WifiManager wiFi;
	public List<ScanResult> lSR;
	public ToneGenerator tG;
	public int level;
	public String bssid;
	
	//TIME MANAGEMENT
	public Date date;
	public long nextMillis;
	
	//PENANDA ADA SINYAL WIFI
	public boolean found;
	
	public boolean active;
	
	
	public SuperWifi(Context context,String bssid){
		//PARAMETER
		wiFi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		tG = new ToneGenerator(AudioManager.STREAM_SYSTEM, 100);
		this.bssid = bssid;

		wiFi.startScan();
		
		nextMillis = 0;
		
		active = true;
		
		found = false;
	}
	
	@Override
	public void run() {
		while(active){
			date = new Date();
			lSR = null;
			lSR = wiFi.getScanResults();
			if(lSR!=null){
				if(date.getTime()>=nextMillis){
					for(ScanResult sR : lSR){
						if(sR.BSSID.contains(bssid)){
							Log.d("Wifi", "ada wifi");
							found = true;
							
							level = sR.level;
//Log.d("Signal",String.valueOf(level));
							//HANDLER LEVEL
							if(level >=-70 && level <-50){
								tG.startTone(ToneGenerator.TONE_DTMF_B,500);
								nextMillis = date.getTime()+500;
							}else if(level >=-50){
								tG.startTone(ToneGenerator.TONE_DTMF_C, 250);
								nextMillis = date.getTime()+250;
							}else{
								tG.startTone(ToneGenerator.TONE_DTMF_A, 1000);
								nextMillis = date.getTime()+1000;
							}
							
							wiFi.startScan();
						}else{
							found = false;
							nextMillis = date.getTime()+3000;
							wiFi.startScan();
						}
					}
				}
			}else{
				found = false;
//Log.d("WW", "WIFI MATI");
			}
		}
	}
	
	public void release(){
		active = false;
	}
	

}
