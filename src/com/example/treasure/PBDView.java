package com.example.treasure;

import android.R.color;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PBDView extends View {

	public static final float LINE_MAX = 100;//BATAS ATAS SENSOR
	public static final float LINE_MIN = 25;//BATAS BAWAH SENSOR
	
	public static final float LINE_WIDTH = 10;//LEBAR GARIS
	
	
	
  private Paint paint;
  private Paint paint1;
  private Paint paint2;
  private float position = 0;

  private double longitude;
  private double latitude;
  private double dist;
  
  public PBDView(Context context) {
    super(context);
    init();
  }

  private void init() {
    paint = new Paint();
    paint.setAntiAlias(true);
    paint.setStrokeWidth(2);
    paint.setTextSize(25);
    paint.setStyle(Paint.Style.STROKE);
    paint.setColor(Color.BLACK);
    
    paint1 = new Paint();
    paint1.setColor(Color.argb(130, 65, 162, 60));
    
    paint2 = new Paint();
    paint2.setColor(Color.RED);
    paint2.setStyle(Paint.Style.STROKE);
    paint2.setStrokeWidth(3);
  }

  @Override
  protected void onDraw(Canvas canvas) {
//TODO
	canvas.drawARGB(150, 255, 128, 255);
	//DRAW BACKGROUND

//position = 0;	
		
    float xPoint = getMeasuredWidth() / 2;
    float yPoint = getMeasuredHeight() / 2;
    
    float radius = (float) (Math.max(xPoint, yPoint) * 0.6);
    
    double sinTheta = Math.sin((double) (-position)/180 *3.143);
    double cosTheta = Math.cos((double)(-position)/180 * 3.143);
    
    float xEnd = (float) (xPoint);
    float yEnd = (float) (yPoint - dist/LINE_MAX *radius);

	canvas.drawCircle(xPoint, yPoint, radius, paint1);
    
	Log.d("Distance VIEW", ""+dist);
	
    //MENCARI PANJANG PANAH
    if(dist<=LINE_MIN){
    	xEnd = (float) (xPoint);
    	yEnd= (float)(yPoint-25*radius/100);
    }else if(dist >LINE_MAX){
    	xEnd = (float) (xPoint);
    	yEnd= (float)(yPoint-radius);
    }
    
    
    //MENGGAMBAR LINGKARAN
    canvas.drawCircle(xPoint, yPoint, radius, paint);
    for(int i = 0;i<4;i++){
    	canvas.drawCircle(xPoint, yPoint, radius*i/4, paint);
    }
    
   // MENGGAMBAR GARIS LINTANG
    for(int i = 0;i<8;i++){
 //   	canvas.drawLine(xPoint, yPoint, (float)((xPoint+Math.sin((double) (45*i))/180 *3.143*radius)), (float)((yPoint-Math.cos((double)(45*i)/180 * 3.143*radius))), paint);
    }
    
    canvas.drawRect(0, 0, getMeasuredWidth(), getMeasuredHeight(), paint);
    
    //GAMBAR GARIS
    //MENGGAMBAR KAKI GARIS
    canvas.drawLine((float) (((xPoint-LINE_WIDTH/2)-xPoint)*cosTheta+xPoint),(float)(((xPoint-LINE_WIDTH/2)-xPoint)*sinTheta+yPoint), xPoint, yPoint, paint2);
    canvas.drawLine((float) (((xPoint+LINE_WIDTH/2)-xPoint)*cosTheta+xPoint), (float)(((xPoint+LINE_WIDTH/2)-xPoint)*sinTheta+yPoint), xPoint, yPoint, paint2);
    
    //MENGGAMBAR BADAN GARIS
    canvas.drawLine((float) (((xPoint-LINE_WIDTH/2)-xPoint)*cosTheta+xPoint),(float)(((xPoint-LINE_WIDTH/2)-xPoint)*sinTheta+yPoint),(float)(((xEnd-LINE_WIDTH/2)-xPoint)*cosTheta-((yEnd+radius/10/2)-yPoint)*sinTheta + xPoint),(float)(((xEnd-LINE_WIDTH/2)-xPoint)*sinTheta+((yEnd+radius/10/2)-yPoint)*cosTheta + yPoint),paint2);
    canvas.drawLine((float) (((xPoint+LINE_WIDTH/2)-xPoint)*cosTheta+xPoint),(float)(((xPoint+LINE_WIDTH/2)-xPoint)*sinTheta+yPoint),(float)(((xEnd+LINE_WIDTH/2)-xPoint)*cosTheta-((yEnd+radius/10/2)-yPoint)*sinTheta + xPoint),(float)(((xEnd+LINE_WIDTH/2)-xPoint)*sinTheta+((yEnd+radius/10/2)-yPoint)*cosTheta + yPoint),paint2);

    //MENGGAMBAR KEPALA GARIS
    canvas.drawLine((float)(((xPoint-LINE_WIDTH/2)-xPoint)*cosTheta-((yEnd+radius/10/2)-yPoint)*sinTheta+xPoint), (float)(((xPoint-LINE_WIDTH/2)-xPoint)*sinTheta+((yEnd+radius/10/2)-yPoint)*cosTheta+yPoint), (float)(((xPoint-LINE_WIDTH)-xPoint)*cosTheta-((yEnd+radius/10/2)-yPoint)*sinTheta+xPoint), (float)(((xPoint-LINE_WIDTH)-xPoint)*sinTheta+((yEnd+radius/10/2)-yPoint)*cosTheta+yPoint), paint2);
    canvas.drawLine((float)(((xPoint+LINE_WIDTH/2)-xPoint)*cosTheta-((yEnd+radius/10/2)-yPoint)*sinTheta+xPoint), (float)(((xPoint+LINE_WIDTH/2)-xPoint)*sinTheta+((yEnd+radius/10/2)-yPoint)*cosTheta+yPoint), (float)(((xPoint+LINE_WIDTH)-xPoint)*cosTheta-((yEnd+radius/10/2)-yPoint)*sinTheta+xPoint), (float)(((xPoint+LINE_WIDTH)-xPoint)*sinTheta+((yEnd+radius/10/2)-yPoint)*cosTheta+yPoint), paint2);
   
    canvas.drawLine((float)(((xPoint-LINE_WIDTH)-xPoint)*cosTheta-((yEnd+radius/10/2)-yPoint)*sinTheta+xPoint), ((float)(((xPoint-LINE_WIDTH)-xPoint)*sinTheta+((yEnd+radius/10/2)-yPoint)*cosTheta+yPoint)), (float)((xEnd-xPoint)*cosTheta-(yEnd-yPoint)*sinTheta+xPoint), (float)((xEnd-xPoint)*sinTheta+(yEnd-yPoint)*cosTheta+yPoint), paint2);
    canvas.drawLine((float)(((xPoint+LINE_WIDTH)-xPoint)*cosTheta-((yEnd+radius/10/2)-yPoint)*sinTheta+xPoint), ((float)(((xPoint+LINE_WIDTH)-xPoint)*sinTheta+((yEnd+radius/10/2)-yPoint)*cosTheta+yPoint)), (float)((xEnd-xPoint)*cosTheta-(yEnd-yPoint)*sinTheta+xPoint), (float)((xEnd-xPoint)*sinTheta+(yEnd-yPoint)*cosTheta+yPoint), paint2);

    
  }

  public void updateData(float position) {
    this.position = position;
    invalidate();
  }
  
  public void updateData(double longitude,double latitude){
	  this.longitude = longitude;
	  this.latitude = latitude;
	  invalidate();
  }

  public void updateData(double dist){
	  this.dist = dist;
	  invalidate();
  }
} 