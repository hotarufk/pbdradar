/**
 * 
 */
package com.example.treasure;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Emon
 *
 */
public class ImageUpload extends Thread {
//Atribute
private static final String URL = "http://milestone.if.itb.ac.id/pbd/index.php";
private String Chest_id;
private File  file;
private String BSSID;
private String WiFi;
private String Answer;
private Context contex;
private static final String group_id ="8c6957c907d84b0d6cf5de5eece5cee3";
boolean b;
String A;
	public ImageUpload(String CI,File Image, String bssid,int wifi,boolean bool,String ans,Context c) {
		// TODO Auto-generated constructor stub
		Chest_id = CI;
		file = Image;
		BSSID = bssid;
		WiFi += wifi;
		b = bool;
		A = ans;
		contex =c;
	}

	@Override
	public void run(){
		try {
			HttpClient client = new DefaultHttpClient();
			Log.d("Submit", "define client");
			HttpPost upload = new HttpPost(URL);
			//masukan yang mau di upload
			
			Log.d("Submit", "set request");
			 FileBody image = new FileBody(file);
			MultipartEntity reqEntity = new MultipartEntity();
			reqEntity.addPart("group id", new StringBody(group_id));
			reqEntity.addPart("chest_id", new StringBody(Chest_id));
			reqEntity.addPart("file",image);
			reqEntity.addPart("bssid", new StringBody(BSSID));
			reqEntity.addPart("wifi",new StringBody(WiFi));
			reqEntity.addPart("action", new StringBody("acquire"));
			//upload file
			Log.d("Submit", "set request");
			upload.setEntity(reqEntity);
			
			
			HttpResponse response;
			Log.d("Submit", "set response");
			response = client.execute(upload);
			Log.d("Submit", "request to server");
			
			// Get the response
			BufferedReader rd = new BufferedReader
			  (new InputStreamReader(response.getEntity().getContent()));
			Log.d("Submit", "get response");
			
			String line = "";
			while ((line = rd.readLine()) != null) {
			  Answer+=line;
			}
			Log.d("Submit Answer", Answer);
			//Toast.makeText(contex, "Server Answer : "+Answer, Toast.LENGTH_LONG).show();
		} catch (ClientProtocolException e) {
	       Log.d("Submit Exception","---" + e.getMessage());
	    } catch (IOException e1) {
	    	Log.d("Submit Exception","---" + e1.getMessage());
	    }
	A= Answer;
	b = false;
	}


}
