package com.example.treasure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import com.example.treasure.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.widget.Toast;

@SuppressLint("NewApi")
public class RadarMain extends Activity implements LocationListener {


private CompassView compassView;
private double Longitude;
private double Latitude;
private ArrayList<String> id = new ArrayList<String>();
private ArrayList<String> bssid = new ArrayList<String>();
private ArrayList<Double> distance = new ArrayList<Double>();
private ArrayList<Double> degree = new ArrayList<Double>();
private LocationManager locationManager;
private String provider;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/*//DUMMY FOR MODE 2
		Intent intent = new Intent(getApplicationContext(), TrackingMode.class);
		
		intent.putExtra("latitude", Latitude);
		intent.putExtra("longitude", Longitude);
		intent.putExtra("bssid", "S");
		intent.putExtra("degree", 2);
		intent.putExtra("distance", 3);
		intent.putExtra("id", "S");
		
		startActivity(intent);*/
		
		  compassView = new CompassView(this);
		  setContentView(compassView);
		  Log.d("on create","set policy");
		  StrictMode.ThreadPolicy policy = new StrictMode.
		  ThreadPolicy.Builder().permitAll().build();
		  Log.d("selesai baca","test 1");
		  StrictMode.setThreadPolicy(policy);
		  Log.d("selesai baca","test 2");  
		// Get the location manager
		    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		    // Define the criteria how to select the locatioin provider -> use
		    // default
		    Criteria criteria = new Criteria();
		    provider = locationManager.getBestProvider(criteria, false);
		    Location location = locationManager.getLastKnownLocation(provider);

		    // Initialize the location fields
		    if (location != null) {
		      System.out.println("Provider " + provider + " has been selected.");
		      onLocationChanged(location);
		    } else {
		    
		    }
		    Log.d("Latitude",""+Latitude);
		    Log.d("Longitude",""+Longitude);
		  Log.d("selesai baca","test 3 clear");
		  postData();
		  Log.d("selesai baca","test 4");
		  //parsing 
		  compassView.updateData(Latitude, Longitude, distance, degree);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.radar_main, menu);
		return true;
		
	}
	
	public boolean onPrepareOptionsMenu(Menu menu) {
		SubMenu sub=menu.getItem(0).getSubMenu();
		sub.clear();
		
		for(int i=0;i<degree.size();i++)
		{
				sub.add(0,i,0,""+(i+1)).setOnMenuItemClickListener(new OnMenuItemClickListener() {
				
				@Override
				public boolean onMenuItemClick(MenuItem p) {
					//CONVERT TO LATITUDE LONGITUDE
					
					Toast.makeText(getApplicationContext(),"Mencari harta "+(p.getItemId()+1), Toast.LENGTH_LONG).show();
					Intent intent = new Intent(getApplicationContext(), TrackingMode.class);
					
					intent.putExtra("latitude", Latitude);
					intent.putExtra("longitude", Longitude);
					intent.putExtra("bssid", bssid.get(p.getItemId()));
					intent.putExtra("degree", degree.get(p.getItemId()));
					intent.putExtra("distance", distance.get(p.getItemId()));
					intent.putExtra("id", id.get(p.getItemId()));
					
					startActivity(intent);
					finish();
					// TODO Auto-generated method stub
					return false;
				}
			});	
		}
	return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
	super.onOptionsItemSelected(item);
	switch(item.getItemId()){
	case R.id.Refresh :
		id.clear();
		bssid.clear();
		distance.clear();
		degree.clear();
		Toast.makeText(getApplicationContext(), "Refresh di lakukan", Toast.LENGTH_LONG).show();
	    Location location = locationManager.getLastKnownLocation(provider);
	    // Initialize the location fields
	    if (location != null) {
	      System.out.println("Provider " + provider + " has been selected.");
	      onLocationChanged(location);
	    } else {
	    
	    }
		postData();
		invalidateOptionsMenu();
		compassView.updateData(Latitude,Longitude,distance,degree);
		if(id.isEmpty())
			Toast.makeText(getApplicationContext(), "Tidak Ada Harta di Sekitar", Toast.LENGTH_LONG).show();
		else
			Toast.makeText(getApplicationContext(), "Ada "+id.size()+" Harta di Sekitar", Toast.LENGTH_LONG).show();
		break;
	case R.id.Tracking :
		Intent intent = new Intent(this,TrackingMode.class);
		if(id.isEmpty()){
			Toast.makeText(this, "ga ada harta", Toast.LENGTH_SHORT).show();
		}else{
//Log.d("Latitude",String.valueOf(Latitude) );
//Log.d("Longitude",String.valueOf(Longitude));
		}
		break;
	case R.id.ChestCount:
		try {
			// Create a new HttpClient and Post Header
		    HttpClient httpclient = new DefaultHttpClient();
		    HttpGet httppost = new HttpGet("http://milestone.if.itb.ac.id/pbd/index.php?group_id=8c6957c907d84b0d6cf5de5eece5cee3&action=number");
		    String test= "a";
		    // Execute HTTP Get Request
		    Log.d("ChestCount","try");
		    HttpResponse response = httpclient.execute(httppost);
		    Log.d("ChestCount","getresponsee");
		    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		    Log.d("ChestCount","begin");
		    String line = "";
		     while ((line = rd.readLine()) != null) {
		    	 System.out.println(line);
		         test = line;
		      }
		     Log.d("ChestCount","end");
		     Toast.makeText(this, test, Toast.LENGTH_LONG).show();
		    } catch (ClientProtocolException e) {
		        // TODO Auto-generated catch block
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		    }

		break;
	case R.id.Restart :
		id.clear();
		bssid.clear();
		distance.clear();
		degree.clear();
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://milestone.if.itb.ac.id/pbd/index.php");
			String test = "a";
			//EXECUTE HTTP GET REQUEST
			Log.d("Restart","try");
			MultipartEntity reqEntity = new MultipartEntity();
			reqEntity.addPart("group_id", new StringBody("8c6957c907d84b0d6cf5de5eece5cee3"));
			reqEntity.addPart("action", new StringBody("reset"));
			httppost.setEntity(reqEntity);
			HttpResponse response = httpclient.execute(httppost);
			Log.d("Restart","getresponsee");
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			Log.d("Restart","begin");
			String line = "";
			while ((line = rd.readLine()) != null) {
				System.out.println(line);
			    test = line;
			}
			Log.d("Restart","end");
			Toast.makeText(this, test, Toast.LENGTH_LONG).show();
			invalidateOptionsMenu();
			compassView.updateData(Latitude,Longitude,distance,degree);
		}catch (ClientProtocolException e) {
		    // TODO Auto-generated catch block
		}catch (IOException e) {
		    // TODO Auto-generated catch block
		};
	break;
	}
		
	return true;	
	}
	
  
	

public void postData() {
try {
	// Create a new HttpClient and Post Header
    HttpClient httpclient = new DefaultHttpClient();
    HttpGet httppost = new HttpGet("http://milestone.if.itb.ac.id/pbd/index.php?group_id=8c6957c907d84b0d6cf5de5eece5cee3&action=retrieve&latitude="+Latitude+"&longitude="+Longitude);
    String test= "a";
    // Execute HTTP Get Request
    Log.d("debug","try");
    HttpResponse response = httpclient.execute(httppost);
    Log.d("getresponse","getresponsee");
    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
    Log.d("getAnswer","begin");
    String line = "";
     while ((line = rd.readLine()) != null) {
    	 System.out.println(line);
         test = line;
      }
     Log.d("getAnswer","end");
   //parsing
     ArrayList<String> temp1=new ArrayList<String>();
     test.replace("//W", "");
     String[] asal=test.split("[{ , \" } ]");
     for (int i=0;i<asal.length;i++) {
     	if(asal[i].length()!=0 && (!asal[i].equals(":")) && (!asal[i].equals("]")))
     	{
     		temp1.add(asal[i]);
     	}
		}
     for (int j=0;j<temp1.size();j++) {
			if(temp1.get(j).equals("id"))
			{
				id.add(temp1.get(j+1));
			}
			if(temp1.get(j).equals("bssid"))
			{
				bssid.add(temp1.get(j+1));
			}
			if(temp1.get(j).equals("distance"))
			{
				distance.add(Double.parseDouble(temp1.get(j+1).substring(1)));
			}
			if(temp1.get(j).equals("degree"))
			{
				degree.add(Double.parseDouble(temp1.get(j+1).substring(1)));
			}
		}
     Log.d("End Parsing","End");
    } catch (ClientProtocolException e) {
        // TODO Auto-generated catch block
    } catch (IOException e) {
        // TODO Auto-generated catch block
    }
}

/* Request updates at startup */
@Override
protected void onResume() {
  super.onResume();
  locationManager.requestLocationUpdates(provider, 400, 1, this);
}

/* Remove the locationlistener updates when Activity is paused */
@Override
protected void onPause() {
  super.onPause();
  locationManager.removeUpdates(this);
}

@Override
public void onLocationChanged(Location location) {
   Latitude = (double) (location.getLatitude());
   Longitude = (double) (location.getLongitude());
}

@Override
public void onStatusChanged(String provider, int status, Bundle extras) {
  // TODO Auto-generated method stub

}

@Override
public void onProviderEnabled(String provider) {
  Toast.makeText(this, "Enabled new provider " + provider,
      Toast.LENGTH_SHORT).show();

}

@Override
public void onProviderDisabled(String provider) {
  Toast.makeText(this, "Disabled provider " + provider,
      Toast.LENGTH_SHORT).show();
}

} 

